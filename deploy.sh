#! /bin/bash

echo "Installing subversion"
apt-get install -y subversion
echo "After installing subversion"

# Main configuration
PLUGINSLUG="cf7-submit-animations"
CURRENTDIR=`pwd`
MAINFILE="cf7-submit-animations.php" # Main php file of the plugin
TRUNKCMTMSG="Pushing files to trunk"

# Git configuration
GITPATH="$CURRENTDIR/" # This file should be in the base of git repo

# Svn configuration
SVNPATH="/tmp/$PLUGINSLUG" # Path to a temp SVN repo. No trailing slash required and don't add trunk.
SVNURL="http://plugins.svn.wordpress.org/cf7-submit-animations/" # Remote SVN repo on wordpress.org, with trailing slash
SVNUSER="greyhatch" # Svn username
SVNPASS=$WPACCPASS


# Start the work...
echo ".........................................."
echo 
echo "Preparing to deploy wordpress plugin"
echo 
echo ".........................................."
echo

# Check if subversion is installed before getting all worked up
if ! which svn >/dev/null; then
	echo "You'll need to install subversion before proceeding. Exiting....";
	exit 1;
fi

NEWVERSION2=`grep "Version:" $GITPATH/$MAINFILE | awk -F' ' '{print $NF}'`
echo "$MAINFILE version: $NEWVERSION2"

cd $GITPATH

echo "Creating local copy of SVN repo ..."
svn co --username=$SVNUSER --password=$SVNPASS $SVNURL $SVNPATH

echo "Exporting the HEAD of master from git to the trunk of SVN"
git checkout-index -a -f --prefix=$SVNPATH/trunk/

echo "Ignoring github specific files and deployment script"
svn propset svn:ignore "deploy.sh
Version-History.txt
screenshots
.git
.gitignore" "$SVNPATH/trunk/"

echo "Changing directory to SVN and committing to trunk"
cd $SVNPATH/trunk/
# Add all new files that are not set to be ignored
svn status | grep -v "^.[ \t]*\..*" | grep "^?" | awk '{print $2}' | xargs svn add
svn commit --username=$SVNUSER --password=$SVNPASS -m "$TRUNKCMTMSG"

echo "Creating new SVN tag & committing it"
cd $SVNPATH
svn copy trunk/ tags/$NEWVERSION2/
cd $SVNPATH/tags/$NEWVERSION2
svn commit --username=$SVNUSER --password=$SVNPASS -m "Tagging version $NEWVERSION2"

echo "Removing temporary directory $SVNPATH"
rm -fr $SVNPATH/

echo "*** FINISHED ***"